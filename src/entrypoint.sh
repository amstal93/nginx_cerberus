#!/bin/sh

if [[ $LOG_STDOUT == False ]] 
then
	# write logs to file instead of STDOUT(remove initial symlinks from Nginx docker image)
	rm -rf /var/log/nginx/access.log
	rm -rf /var/log/nginx/error.log
fi 

if [[ $AUTO_HTTPS == True ]] 
then
	for d in $(grep server_name /etc/nginx/conf.d/*.conf -iI | sed -E 's/server_name(\s\t)*//g' | sed -E 's/(\s\t)*;//g'); do
		echo "switching ${d} to HTTPS"
		certbot --nginx --non-interactive --agree-tos --email $ADMIN_EMAIL --domains $d --expand --redirect
		echo "0 13 5 * * /usr/bin/certbot renew --dry-run" | tee -a /etc/crontabs/root > /dev/null
	done
fi

exec nginx -g 'daemon off;'

exec "$@"